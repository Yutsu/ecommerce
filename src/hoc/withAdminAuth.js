import { useAdminAuth } from './../customHooks';

//render children
const WithAdminAuth = props => useAdminAuth() && props.children;

export default WithAdminAuth;