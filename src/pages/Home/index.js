import React from 'react';
import Directory from '../../components/Directory';
import "./styles.scss";

const Home = () => {
    return (
        <section className="home">
            <Directory />
        </section>
    );
};

export default Home;