import React, { useState } from 'react';
import { useDispatch } from 'react-redux'
import { addProductStart } from './../../redux/Products/products.actions';
import Modal from './../../components/Modal';
import FormInput from './../../components/Forms/FormInput';
import FormSelect from './../../components/Forms/FormSelect';
import Button from './../../components/Forms/Button';
import './styles.scss';

const Admin = () => {
    const dispatch = useDispatch()
    const [hideModal, setHideModal] = useState(true);
    const [productCategory, setProductCategory] = useState('apple');
    const [productName, setProductName] = useState('');
    const [productThumbnail, setProductThumbnail] = useState('');
    const [productPrice, setProductPrice] = useState(0);

    const toggleModal = () => setHideModal(!hideModal);

    const configModal = {
        hideModal,
        toggleModal
    };

;
    const handleSubmit = e => {
        e.preventDefault();
        dispatch(addProductStart({
            productCategory,
            productName,
            productThumbnail,
            productPrice
        }))
    };

    return (
        <div className="admin">
        <div className="callToActions">
            <ul>
                <li>
                    <Button onClick={() => toggleModal()}>
                        Add new product | {productCategory} or Samsung
                    </Button>
                </li>
            </ul>
        </div>

        <Modal {...configModal}>
            <div className="addNewProductForm">
            <form onSubmit={handleSubmit}>
                <h2>Add new product</h2>
                <FormSelect
                    label="Category"
                    options={[{
                        value: "apple",
                        name: "apple"
                    }, {
                        value: "samsung",
                        name: "samsung"
                    }]}
                    handleChange={e => setProductCategory(e.target.value)}
                />

                <FormInput
                    label="Name"
                    type="text"
                    value={productName}
                    handleChange={e => setProductName(e.target.value)}
                />

                <FormInput
                    label="Main image URL"
                    type="url"
                    value={productThumbnail}
                    handleChange={e => setProductThumbnail(e.target.value)}
                />

                <FormInput
                    label="Price"
                    type="number"
                    min="0.00"
                    max="10000.00"
                    step="0.01"
                    value={productPrice}
                    handleChange={e => setProductPrice(e.target.value)}
                />
                <Button type="submit">
                    Add product
                </Button>
            </form>
            </div>
        </Modal>
        </div>
    );
}

export default Admin;