import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { auth, handleUserProfile } from './firebase/utils';
import { setCurrentUser } from './redux/User/user.actions';

//Components
import AdminToolbar from './components/AdminToolbar';

//Hoc
import WithAuth from "./hoc/withAuth";
import WithAdminAuth from './hoc/withAdminAuth';

//Layouts
import AdminLayout from './layouts/AdminLayout';
import DashBoardLayout from './layouts/DashboardLayout';
import HomeLayout from './layouts/HomeLayout';
import MainLayout from './layouts/MainLayout';

//Pages
import Home from './pages/Home';
import Login from './pages/Login';
import Registration from './pages/Registration';
import Dashboard from './pages/Dashboard';
import Admin from './pages/Admin';

import './settings.scss';

const App = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        const authListener = auth.onAuthStateChanged(async(userAuth) => {
            if(userAuth){
                const userRef = await handleUserProfile(userAuth);
                userRef.onSnapshot(snapshot => {
                    dispatch(setCurrentUser({
                        id: snapshot.id,
                        ...snapshot.data()
                    }))
                })
            }
            dispatch(setCurrentUser(userAuth));
        });

        //componentWillUnmont
        return () => {
            authListener();
        }
    }, [dispatch])

    return (
        <div className="App">
            <AdminToolbar />
            <Switch>
                <Route exact path="/" render={() => (
                    <HomeLayout>
                        <Home />
                    </HomeLayout>
                )}/>

                <Route path="/registration" render={() => (
                    <MainLayout>
                        <Registration />
                    </MainLayout>
                )}/>

                <Route path="/login" render={() => (
                    <MainLayout>
                        <Login />
                    </MainLayout>
                )}/>

                <Route path="/dashboard" render={() => (
                    <WithAuth>
                        <DashBoardLayout>
                            <Dashboard />
                        </DashBoardLayout>
                    </WithAuth>
                )}/>

                <Route path="/admin" render={() => (
                    <WithAdminAuth>
                        <AdminLayout>
                                <Admin />
                        </AdminLayout>
                    </WithAdminAuth>
                )}/>
            </Switch>
        </div>
    );
}

export default App;
