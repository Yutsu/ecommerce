import React from 'react';
import Apple from "./assets/shopApple.jpg";
import Samsung from "./assets/shopSamsung.jpg";
import './styles.scss';

const Directory = () => {
    return (
        <div className="directory">
            <div className="container">
                <div className="item"
                    style={{
                        backgroundImage: `url(${Apple})`
                    }}>
                    <p className="shopText">Shop Apple</p>
                </div>
                <div className="item"
                    style={{
                        backgroundImage: `url(${Samsung})`
                    }}>
                    <p className="shopText">Shop Samsung</p>
                </div>
            </div>
        </div>
    );
};

export default Directory;