import React, { useState, useEffect } from 'react';
import Button from '../Forms/Button';
import { useDispatch, useSelector } from 'react-redux';
import { signInUser, signInWithGoogle, resetAllAuthForms } from './../../redux/User/user.actions';
import { withRouter } from 'react-router-dom';
import './styles.scss';
import FormInput from '../Forms/FormInput';

const mapState = ({user}) => ({
    signInSuccess: user.signInSuccess
});

const SignIn = (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();
    const { signInSuccess } = useSelector(mapState);

    useEffect(() => {
        if(signInSuccess){
            resetForm();
            dispatch(resetAllAuthForms());
            props.history.push('/');
        }
    }, [signInSuccess, props.history, dispatch])

    const resetForm = () => {
        setEmail(null);
        setPassword('');
    };

    const handleFormSubmit = (e) => {
        e.preventDefault();
        dispatch(signInUser({
            email,
            password
        }));
    }

    const handleGoogleSignIn = () => {
        dispatch(signInWithGoogle());
    }

    return (
        <div className="signin">
            <div className="container">
                <h2> Login </h2>
                <div className="form_container">
                    <form onSubmit={handleFormSubmit}>
                        <FormInput
                            type="email"
                            name="email"
                            value={email}
                            placeholder='Email'
                            handleChange={e => setEmail(e.target.value)}
                        />
                        <FormInput
                            type="password"
                            name="password"
                            value={password}
                            placeholder='Password'
                            handleChange={e => setPassword(e.target.value)}
                        />
                        <Button type="submit">
                            Confirm
                        </Button>
                        <div className="socialSignin">
                            <div className="row">
                                <Button onClick={handleGoogleSignIn}>
                                    SignIn in with Google
                                </Button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default withRouter(SignIn);
