import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { auth } from './../../firebase/utils'
import "./styles.scss";

const mapState = ({user}) => ({
    currentUser: user.currentUser
});

const Header = () => {
    const { currentUser } = useSelector(mapState);

    return (
        <header>
            <div className="container">
                <div className="logo">
                    <Link to="/">
                        <img src="/assets/shopping.png" alt="ecommerce logo react"/>
                        <p>ShopyCommerce</p>
                    </Link>
                </div>
                <div className="callToActions">
                    {currentUser && (
                        <ul>
                            <li>
                                <Link to="/dashboard">
                                    My account
                                </Link>
                            </li>
                            <li>
                                <span onClick={() => auth.signOut()}>
                                    Logout
                                </span>
                            </li>
                        </ul>
                    )}
                    {!currentUser && (
                        <ul>
                            <li>
                                <Link to="/registration">
                                    Register
                                </Link>
                            </li>
                            <li>
                                <Link to="/login">
                                    Login
                                </Link>
                            </li>
                        </ul>
                    )}
                </div>
            </div>
        </header>
    );
};

Header.defaultProps = {
    currentUser: null
}

export default Header;
