import React from 'react';
import './styles.scss';

const FormInput = ({handleChange, label, ...props}) => {
    return (
        <div className="formRow">
            {label && (
                <label>
                    {label}
                </label>
            )}
            <input className="formInput" onChange={handleChange} { ...props} />
        </div>
    );
};

export default FormInput;