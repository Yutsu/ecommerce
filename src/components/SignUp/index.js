import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { resetAllAuthForms, signUpUser } from './../../redux/User/user.actions';
import { withRouter } from 'react-router-dom';
import Button from '../Forms/Button';
import FormInput from '../Forms/FormInput';
import './styles.scss';

const mapState = ({user}) => ({
    signUpSuccess: user.signUpSuccess,
    signUpError: user.signUpError
})

const SignUp = (props) => {
    const [ displayName, setDisplayName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ confirmPassword, setConfirmPassword ] = useState('');
    const [ errors, setErrors ] = useState([]);

    const dispatch = useDispatch();
    const { signUpSuccess, signUpError } = useSelector(mapState);

    useEffect(() => {
        if(signUpSuccess) {
            resetForm();
            props.history.push('/');
            dispatch(resetAllAuthForms())
        }
    }, [signUpSuccess, props.history, dispatch]);

    useEffect(()=> {
        if(Array.isArray(signUpError) && signUpError.length > 0){
            setErrors(signUpError)
        }
    }, [signUpError])

    const resetForm = () => {
        setDisplayName('');
        setEmail('');
        setPassword('');
        setConfirmPassword('');
        setErrors('');
    };

    const handleFormSubmit = (e) => {
        e.preventDefault();
        dispatch(signUpUser({
            displayName,
            email,
            password,
            confirmPassword
        }))
    }

    return (
        <div className="signup">
            <div className="container">
                <h2>SignUp</h2>
                {errors.length > 0 && (
                    <ul>
                        {errors.map((error, index) => {
                            return(
                                <li key={index}>
                                    {error}
                                </li>
                                )
                            })}
                    </ul>
                )}
                <div className="formWrap">
                    <form onSubmit={handleFormSubmit}>
                        <FormInput
                            type="text"
                            name="displayName"
                            value={displayName}
                            placeholder="Fullname"
                            handleChange={e=> setDisplayName(e.target.value)}
                        />
                        <FormInput
                            type="teemailxt"
                            name="email"
                            value={email}
                            placeholder="Email"
                            handleChange={e => setEmail(e.target.value)}
                        />
                        <FormInput
                            type="password"
                            name="password"
                            value={password}
                            placeholder="Password"
                            handleChange={e => setPassword(e.target.value)}
                        />
                        <FormInput
                            type="password"
                            name="confirmPassword"
                            value={confirmPassword}
                            placeholder="Confirm the password"
                            handleChange={e => setConfirmPassword(e.target.value)}
                        />
                        <Button type="submit">
                            Register
                        </Button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default withRouter(SignUp);